package com.example.citasrandom.ui

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.citasrandom.data.Repository
import com.example.citasrandom.data.model.QuoteModel

class QuotesViewModel : ViewModel() {

    private val repository = Repository()
    val randomLiveData = MutableLiveData<QuoteModel>()
    val listaLiveData = MutableLiveData<List<QuoteModel>>()

//    init {
//        actualizarCita()
//    }

    fun actualizarCita() {
        randomLiveData.value = repository.random()
    }

    fun dameLista() {
        listaLiveData.value = repository.listaCompleta()
    }


}