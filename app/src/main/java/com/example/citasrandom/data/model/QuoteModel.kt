package com.example.citasrandom.data.model

data class QuoteModel(
    val quote: String,
    val author: String
    )
