package com.example.citasrandom.data

import com.example.citasrandom.data.model.QuoteModel

class Repository {

    fun random(): QuoteModel {
        val position = (0 until quotes.size).random()
        return quotes[position]
    }
    fun listaCompleta(): List<QuoteModel> {
        return quotes
    }

    private val quotes = listOf(
        QuoteModel(
            quote = "La vida es una obra teatral que no importa cuánto haya durado, sino lo bien que haya sido representada",
            author = "Sémeca"
        ),
        QuoteModel(
            quote = "Nuestra mayor gloria no es no caer nunca, sino levantarnos cada vez que caemos.",
            author = "Confucio"
        ),
        QuoteModel(
            quote = "Nunca eres demasiado viejo para establecer un nuevo objetivo o para soñar un nuevo sueño.",
            author = "C. S. Lewis"
        ),
        QuoteModel(
            quote = "Donde hay amor, hay vida.",
            author = "Mahatma Gandhi"
        ),
        QuoteModel(
            quote = "Elige un trabajo que ames, y no tendrás que trabajar un solo día de tu vida.",
            author = "Confucio"
        ),
        QuoteModel(
            quote = "El que busca un amigo sin defectos se queda sin amigos.",
            author = "Anónimo"
        ),
        QuoteModel(
            quote = "Debes de estar dispuesto a ser un principiante cada una de tus mañanas.",
            author = "Meister Eckhart"
        ),
        QuoteModel(quote = "La educación no es preparación para la vida; la educación es la vida en sí misma.", author = "Jhon Dewey"),
        QuoteModel(quote = "Cualquier tonto puede saber. La clave está en entender.", author = "Albert Einstein"),
        QuoteModel(
            quote = "No cuentes los días, haz que los días cuenten.",
            author = "Muhamed Ali"
        ),
        QuoteModel(quote = "Tu actitud, no tu aptitud, determinará tu altitud.", author = "Zig Ziglar"),
        QuoteModel("Más vale tarde que nunca.","Libanius"),
        QuoteModel("Cuanto más grande es la prueba, más glorioso es el triunfo.","William Shakespeare"),
        QuoteModel("Lo que no me mata, me alimenta.","Frida Kahlo"),
        QuoteModel("El arte de vencer se aprende en las derrotas.","Simón Bolívar"),
        QuoteModel("Hagas lo que hagas, hazlo bien.","Abraham Lincoln"),
        QuoteModel("Piensa, Sueña, Cree y Atrévete.","Walt disney"),
    )
}